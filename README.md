# Intro

This is an implementation of theScore challenge ([the Rush](https://github.com/tsicareers/nfl-rushing)). 

Given that theScore internally uses the `PETAL` stack and my previous (very light!) experience with this stack I've opted to use this opportunity to refresh my knowledge a bit and experience LiveView a bit further.

I'm in no way an Elixir expert though, I've at most used it on a couple of projects and it's been over a year since I last had an opportunity to use it, but I'm definitely a fan of Elixir and just the plain joy of using it has made this worth it for me!

There are lots of improvements that I would like to see implemented given more time, some of which are listed below:
  * test coverage - this is a pretty basic implementation so I haven't focused on that much, but definitely something I would expect to be covered on a live project
  * exception handling - again, same thing as above
  * UI/UX - again, same thing as above
  * edge cases handling
  * cleanup - Phoenix brings a lot of functionality out of the box, I opted for adding what was needed for this challenge and keep everything else, again, due to time constraints; eg: routes, views, templates, etc..
  * environment configurations - opted for keeping credentials in code for sake of simplicity when sharing the code base for review but obviously things like database credentials would be kept in environment variables on a regular project
  * method anotations
  * and so much more...

All that said, I'm excited to hear some feedback on this project and I hope some details help communicating the care I typically put on all my projects, namingly:
  * data cleansing - spent some time reviewing the data to make sure all data is cleaned up and loaded correctly
  * full text search; though the requirement is for just using the player's name the setup is ready to handle any additional searchable fields 
  * CSV download from within a LiveView, using the exact same parameters as the view for consistency
  * Paginating the results for increased scalability and UX

# Installing and running this solution

## Requirements
  * Elixir
  * Docker

## Installation
  1. Clone this repo to your local
  2. Start the PostgreSQL server with `docker-compose up`
  3. Install dependencies with `mix deps.get`
  4. Create and migrate your database with `mix ecto.setup`
  6. Start Phoenix endpoint with `mix phx.server` 
  7. There's no step #7, that's it, you should be good to go! Open `http://localhost:4000` in your browser to give it a spin.

