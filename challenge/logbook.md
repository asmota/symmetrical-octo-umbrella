## Steps

1. [X] Analyze the data to define a schema
2. [X] Learn about the stats to better understand what it should look like and inform the schema decisions
3. [X] Build live view foundations:
   1. [X] live template
   2. [X] added sorting functionality 
   3. [X] added filtering functionality
4. [X] Add tailwind to project
5. [X] Look and feel
6. [X] CSV download 
7. [X] add unique index on name / team? & fields used for sorting
8. [X] split the touchdown variable 
9.  [ ] fix pagination
10. [ ] fix bug with some results not showing up?
11. [ ] no results found..
12. [ ] loading..
13. [ ] Add running instructions

## Potential Improvements

1. Seeding the data could use a data stream to read file if file too large to read into memory
2. Use ETS structure as "cache" for data? (does it allow for indexes?)
3. Add tests
4. pagination
5. deployment? (kubernetes?)
