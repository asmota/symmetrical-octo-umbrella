defmodule Nflrushing.Repo.Migrations.AddStatsIndexes do
  use Ecto.Migration

  def change do
    create(index(:player_stats, [:yds]))
    create(index(:player_stats, [:lng]))
    create(index(:player_stats, [:td]))
  end
end
