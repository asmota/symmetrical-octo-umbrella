defmodule Nflrushing.Repo.Migrations.CreateFulltextIndex do
  @moduledoc """
  Create postgres extension and indices
  """

  use Ecto.Migration

  def up do
    execute("CREATE EXTENSION pg_trgm")

    execute("""
    CREATE INDEX player_stats_trgm_idx ON player_stats USING GIN (to_tsvector('english',
      player))
    """)
  end

  def down do
    execute("DROP INDEX player_stats_trgm_idx")
    execute("DROP EXTENSION pg_trgm")
  end
end
