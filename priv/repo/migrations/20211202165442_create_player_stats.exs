defmodule Nflrushing.Repo.Migrations.CreatePlayerStats do
  use Ecto.Migration

  def change do
    create table(:player_stats) do
      add(:player, :string)
      add(:team, :string)
      add(:pos, :string)
      add(:att, :integer)
      add(:att_g, :decimal)
      add(:yds, :integer)
      add(:avg, :decimal)
      add(:yds_g, :decimal)
      add(:td, :integer)
      add(:lng, :string)
      add(:lng_num, :integer)
      add(:first, :decimal)
      add(:first_percentage, :decimal)
      add(:twenty_plus, :integer)
      add(:forty_plus, :integer)
      add(:fum, :integer)

      timestamps()
    end
  end
end
