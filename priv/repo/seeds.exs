alias Nflrushing.{Repo, Player.PlayerStats}

defmodule Seed do
  def clean_data(data) do
    data
    |> Enum.map(fn a ->
      %{
        a
        | "Yds" => ensure_int(a["Yds"]),
          "Lng" => ensure_string(a["Lng"])
      }
    end)
  end

  defp ensure_int(val) when is_binary(val) do
    val |> String.replace(",", "") |> String.to_integer()
  end

  defp ensure_int(val), do: val

  defp ensure_string(val) when not is_binary(val) do
    val |> Integer.to_string()
  end

  defp ensure_string(val), do: val
end

with {:ok, file_content} <- File.read("./challenge/input.json") do
  file_content
  |> Jason.decode!()
  |> Seed.clean_data()
  |> Enum.map(fn raw ->
    Repo.insert!(
      PlayerStats.changeset(%PlayerStats{}, %{
        player: raw["Player"],
        team: raw["Team"],
        pos: raw["Pos"],
        att: raw["Att"],
        att_g: raw["Att/G"],
        yds: raw["Yds"],
        avg: raw["Avg"],
        yds_g: raw["Yds/G"],
        td: raw["TD"],
        lng: raw["Lng"],
        lng_num: raw["Lng_num"],
        first: raw["1st"],
        first_percentage: raw["1st%"],
        twenty_plus: raw["20+"],
        forty_plus: raw["40+"],
        fum: raw["FUM"]
      })
    )
  end)
end
