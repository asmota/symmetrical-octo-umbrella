defmodule Nflrushing.PlayerTest do
  use Nflrushing.DataCase

  alias Nflrushing.Player

  describe "player_stats" do
    alias Nflrushing.Player.PlayerStats

    import Nflrushing.PlayerFixtures

    @invalid_attrs %{
      att: nil,
      att_g: nil,
      avg: nil,
      first: nil,
      first_percentage: nil,
      forty_plus: nil,
      fum: nil,
      lng: nil,
      player: nil,
      pos: nil,
      td: nil,
      team: nil,
      twenty_plus: nil,
      yds: nil,
      yds_g: nil
    }

    test "list_player_stats/0 returns all player_stats" do
      player_stats = player_stats_fixture()
      assert Player.list_player_stats() == [player_stats]
    end

    test "get_player_stats!/1 returns the player_stats with given id" do
      player_stats = player_stats_fixture()
      assert Player.get_player_stats!(player_stats.id) == player_stats
    end

    test "create_player_stats/1 with valid data creates a player_stats" do
      valid_attrs = %{
        att: 42,
        att_g: "120.5",
        avg: "120.5",
        first: "120.5",
        first_percentage: "120.5",
        forty_plus: 42,
        fum: 42,
        lng: "some lng",
        player: "some player",
        pos: "some pos",
        td: 42,
        team: "some team",
        twenty_plus: 42,
        yds: 42,
        yds_g: "120.5"
      }

      assert {:ok, %PlayerStats{} = player_stats} = Player.create_player_stats(valid_attrs)
      assert player_stats.att == 42
      assert player_stats.att_g == Decimal.new("120.5")
      assert player_stats.avg == Decimal.new("120.5")
      assert player_stats.first == Decimal.new("120.5")
      assert player_stats.first_percentage == Decimal.new("120.5")
      assert player_stats.forty_plus == 42
      assert player_stats.fum == 42
      assert player_stats.lng == "some lng"
      assert player_stats.player == "some player"
      assert player_stats.pos == "some pos"
      assert player_stats.td == 42
      assert player_stats.team == "some team"
      assert player_stats.twenty_plus == 42
      assert player_stats.yds == 42
      assert player_stats.yds_g == Decimal.new("120.5")
    end

    test "create_player_stats/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Player.create_player_stats(@invalid_attrs)
    end

    test "update_player_stats/2 with valid data updates the player_stats" do
      player_stats = player_stats_fixture()

      update_attrs = %{
        att: 43,
        att_g: "456.7",
        avg: "456.7",
        first: "456.7",
        first_percentage: "456.7",
        forty_plus: 43,
        fum: 43,
        lng: "some updated lng",
        player: "some updated player",
        pos: "some updated pos",
        td: 43,
        team: "some updated team",
        twenty_plus: 43,
        yds: 43,
        yds_g: "456.7"
      }

      assert {:ok, %PlayerStats{} = player_stats} =
               Player.update_player_stats(player_stats, update_attrs)

      assert player_stats.att == 43
      assert player_stats.att_g == Decimal.new("456.7")
      assert player_stats.avg == Decimal.new("456.7")
      assert player_stats.first == Decimal.new("456.7")
      assert player_stats.first_percentage == Decimal.new("456.7")
      assert player_stats.forty_plus == 43
      assert player_stats.fum == 43
      assert player_stats.lng == "some updated lng"
      assert player_stats.player == "some updated player"
      assert player_stats.pos == "some updated pos"
      assert player_stats.td == 43
      assert player_stats.team == "some updated team"
      assert player_stats.twenty_plus == 43
      assert player_stats.yds == 43
      assert player_stats.yds_g == Decimal.new("456.7")
    end

    test "update_player_stats/2 with invalid data returns error changeset" do
      player_stats = player_stats_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Player.update_player_stats(player_stats, @invalid_attrs)

      assert player_stats == Player.get_player_stats!(player_stats.id)
    end

    test "delete_player_stats/1 deletes the player_stats" do
      player_stats = player_stats_fixture()
      assert {:ok, %PlayerStats{}} = Player.delete_player_stats(player_stats)
      assert_raise Ecto.NoResultsError, fn -> Player.get_player_stats!(player_stats.id) end
    end

    test "change_player_stats/1 returns a player_stats changeset" do
      player_stats = player_stats_fixture()
      assert %Ecto.Changeset{} = Player.change_player_stats(player_stats)
    end
  end
end
