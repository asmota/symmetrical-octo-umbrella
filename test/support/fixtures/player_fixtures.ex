defmodule Nflrushing.PlayerFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Nflrushing.Player` context.
  """

  @doc """
  Generate a player_stats.
  """
  def player_stats_fixture(attrs \\ %{}) do
    {:ok, player_stats} =
      attrs
      |> Enum.into(%{
        att: 42,
        att_g: "120.5",
        avg: "120.5",
        first: "120.5",
        first_percentage: "120.5",
        forty_plus: 42,
        fum: 42,
        lng: "some lng",
        player: "some player",
        pos: "some pos",
        td: 42,
        team: "some team",
        twenty_plus: 42,
        yds: 42,
        yds_g: "120.5"
      })
      |> Nflrushing.Player.create_player_stats()

    player_stats
  end
end
