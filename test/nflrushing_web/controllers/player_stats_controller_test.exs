defmodule NflrushingWeb.PlayerStatsControllerTest do
  use NflrushingWeb.ConnCase

  import Nflrushing.PlayerFixtures

  @create_attrs %{
    att: 42,
    att_g: "120.5",
    avg: "120.5",
    first: "120.5",
    first_percentage: "120.5",
    forty_plus: 42,
    fum: 42,
    lng: "some lng",
    player: "some player",
    pos: "some pos",
    td: 42,
    team: "some team",
    twenty_plus: 42,
    yds: 42,
    yds_g: "120.5"
  }
  @update_attrs %{
    att: 43,
    att_g: "456.7",
    avg: "456.7",
    first: "456.7",
    first_percentage: "456.7",
    forty_plus: 43,
    fum: 43,
    lng: "some updated lng",
    player: "some updated player",
    pos: "some updated pos",
    td: 43,
    team: "some updated team",
    twenty_plus: 43,
    yds: 43,
    yds_g: "456.7"
  }
  @invalid_attrs %{
    att: nil,
    att_g: nil,
    avg: nil,
    first: nil,
    first_percentage: nil,
    forty_plus: nil,
    fum: nil,
    lng: nil,
    player: nil,
    pos: nil,
    td: nil,
    team: nil,
    twenty_plus: nil,
    yds: nil,
    yds_g: nil
  }

  describe "index" do
    test "lists all player_stats", %{conn: conn} do
      conn = get(conn, Routes.player_stats_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Player stats"
    end
  end

  describe "new player_stats" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.player_stats_path(conn, :new))
      assert html_response(conn, 200) =~ "New Player stats"
    end
  end

  describe "create player_stats" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.player_stats_path(conn, :create), player_stats: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.player_stats_path(conn, :show, id)

      conn = get(conn, Routes.player_stats_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Player stats"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.player_stats_path(conn, :create), player_stats: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Player stats"
    end
  end

  describe "edit player_stats" do
    setup [:create_player_stats]

    test "renders form for editing chosen player_stats", %{conn: conn, player_stats: player_stats} do
      conn = get(conn, Routes.player_stats_path(conn, :edit, player_stats))
      assert html_response(conn, 200) =~ "Edit Player stats"
    end
  end

  describe "update player_stats" do
    setup [:create_player_stats]

    test "redirects when data is valid", %{conn: conn, player_stats: player_stats} do
      conn =
        put(conn, Routes.player_stats_path(conn, :update, player_stats),
          player_stats: @update_attrs
        )

      assert redirected_to(conn) == Routes.player_stats_path(conn, :show, player_stats)

      conn = get(conn, Routes.player_stats_path(conn, :show, player_stats))
      assert html_response(conn, 200) =~ "some updated lng"
    end

    test "renders errors when data is invalid", %{conn: conn, player_stats: player_stats} do
      conn =
        put(conn, Routes.player_stats_path(conn, :update, player_stats),
          player_stats: @invalid_attrs
        )

      assert html_response(conn, 200) =~ "Edit Player stats"
    end
  end

  describe "delete player_stats" do
    setup [:create_player_stats]

    test "deletes chosen player_stats", %{conn: conn, player_stats: player_stats} do
      conn = delete(conn, Routes.player_stats_path(conn, :delete, player_stats))
      assert redirected_to(conn) == Routes.player_stats_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.player_stats_path(conn, :show, player_stats))
      end
    end
  end

  defp create_player_stats(_) do
    player_stats = player_stats_fixture()
    %{player_stats: player_stats}
  end
end
