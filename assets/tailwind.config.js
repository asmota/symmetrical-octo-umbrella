module.exports = {
  mode: "jit",
  purge: ["./js/**/*.js", "../lib/*_web/**/*.*ex"],
  theme: {
    extend: {
      colors: {
        "dark-gray": "#1e1f21",
      },
    },
  },
  variants: {},
  plugins: [require("@tailwindcss/forms")],
};
