defmodule NflrushingWeb.CsvController do
  use NflrushingWeb, :controller

  alias Nflrushing.Player.StatsSearch

  def export(conn, params) do
    conn =
      conn
      |> put_resp_content_type("text/csv")
      |> put_resp_header("content-disposition", "attachment; filename=\"stats.csv\"")
      |> send_chunked(:ok)

    conn |> chunk(set_csv_headers())

    StatsSearch.run_with_stream(params, fn stream ->
      for stat <- stream do
        csv_rows =
          NimbleCSV.RFC4180.dump_to_iodata([
            [
              stat.player,
              stat.team,
              stat.pos,
              stat.att,
              stat.att_g,
              stat.yds,
              stat.avg,
              stat.yds_g,
              stat.td,
              stat.lng,
              stat.first,
              stat.first_percentage,
              stat.twenty_plus,
              stat.forty_plus,
              stat.fum
            ]
          ])

        conn |> chunk(csv_rows)
      end
    end)

    conn
  end

  defp set_csv_headers() do
    NimbleCSV.RFC4180.dump_to_iodata([
      [
        "Player",
        "Team",
        "Pos",
        "Att",
        "Att g",
        "Yds",
        "Avg",
        "Yds g",
        "Td",
        "Lng",
        "1st",
        "1st %",
        "20+",
        "40+",
        "Fum"
      ]
    ])
  end
end
