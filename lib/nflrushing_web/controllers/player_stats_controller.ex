defmodule NflrushingWeb.PlayerStatsController do
  use NflrushingWeb, :controller

  alias Nflrushing.Player

  def index(conn, _params) do
    player_stats = Player.list_player_stats()
    render(conn, "index.html", player_stats: player_stats)
  end
end
