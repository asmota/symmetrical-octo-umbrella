defmodule NflrushingWeb.Stats do
  use NflrushingWeb, :live_view

  import Ecto.Changeset
  alias Nflrushing.Player.StatsSearch

  @impl true
  def mount(params, _user, socket) do
    %{
      entries: entries,
      page_number: page_number,
      page_size: page_size,
      total_entries: total_entries,
      total_pages: total_pages
    } =
      if connected?(socket) do
        StatsSearch.run(params)
      else
        %Scrivener.Page{}
      end

    assigns = [
      conn: socket,
      player_stats: entries,
      page: page_number || 0,
      page_size: page_size || 0,
      total_entries: total_entries || 0,
      total_pages: total_pages || 0,
      order_and_filter_params: order_and_filter_changeset(params)
    ]

    {:ok, assign(socket, assigns)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {
      :noreply,
      socket
      |> assign(:order_and_filter_params, order_and_filter_changeset(params))
      |> get_and_assign_page(params)
    }
  end

  @impl true
  def handle_event("order_and_filter", %{"order_and_filter" => order_and_filter_params}, socket) do
    order_and_filter_params
    |> order_and_filter_changeset()
    |> case do
      %{valid?: true} = changeset ->
        {
          :noreply,
          socket
          |> push_patch(
            to: Routes.live_path(socket, NflrushingWeb.Stats, apply_changes(changeset))
          )
        }

      _ ->
        {:noreply, socket}
    end
  end

  def handle_event(
        "nav",
        %{"page" => page},
        socket
      ) do
    current_params = socket.assigns.order_and_filter_params |> apply_changes()

    {
      :noreply,
      socket
      |> push_patch(
        to:
          Routes.live_path(
            socket,
            NflrushingWeb.Stats,
            current_params |> Map.merge(%{page: page})
          )
      )
    }
  end

  defp order_and_filter_changeset(attrs) do
    cast(
      {%{}, %{order_by: :string, name: :string, page: :integer}},
      attrs,
      [:order_by, :name, :page]
    )
  end

  def get_and_assign_page(socket, params) do
    %{
      entries: entries,
      page_number: page_number,
      page_size: page_size,
      total_entries: total_entries,
      total_pages: total_pages
    } = StatsSearch.run(params)

    socket
    |> assign(:player_stats, entries)
    |> assign(:page, page_number)
    |> assign(:page_size, page_size)
    |> assign(:total_entries, total_entries)
    |> assign(:total_pages, total_pages)
  end
end
