defmodule Nflrushing.Repo do
  use Ecto.Repo,
    otp_app: :nflrushing,
    adapter: Ecto.Adapters.Postgres

  use Scrivener, page_size: 20
end
