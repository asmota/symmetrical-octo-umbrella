defmodule Nflrushing.Player do
  @moduledoc """
  The Player context.
  """

  import Ecto.Query, warn: false
  alias Nflrushing.{Repo, Player.PlayerStats}

  @doc """
  Returns the list of player_stats.

  ## Examples

      iex> list_player_stats()
      [%PlayerStats{}, ...]

  """
  def list_player_stats(params \\ %{}) do
    from(
      s in PlayerStats,
      order_by: ^filter_order_by(params["order_by"])
    )
    |> Repo.all()
  end

  defp filter_order_by("yds_desc"), do: [desc: :yds]
  defp filter_order_by("yds_asc"), do: [asc: :yds]
  defp filter_order_by("lng_desc"), do: [desc: :lng]
  defp filter_order_by("lng_asc"), do: [asc: :lng]
  defp filter_order_by("td_desc"), do: [desc: :td]
  defp filter_order_by("td_asc"), do: [asc: :td]
  defp filter_order_by(_), do: []

  @doc """
  Gets a single player_stats.

  Raises `Ecto.NoResultsError` if the Player stats does not exist.

  ## Examples

      iex> get_player_stats!(123)
      %PlayerStats{}

      iex> get_player_stats!(456)
      ** (Ecto.NoResultsError)

  """
  def get_player_stats!(id), do: Repo.get!(PlayerStats, id)

  @doc """
  Creates a player_stats.

  ## Examples

      iex> create_player_stats(%{field: value})
      {:ok, %PlayerStats{}}

      iex> create_player_stats(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_player_stats(attrs \\ %{}) do
    %PlayerStats{}
    |> PlayerStats.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a player_stats.

  ## Examples

      iex> update_player_stats(player_stats, %{field: new_value})
      {:ok, %PlayerStats{}}

      iex> update_player_stats(player_stats, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_player_stats(%PlayerStats{} = player_stats, attrs) do
    player_stats
    |> PlayerStats.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a player_stats.

  ## Examples

      iex> delete_player_stats(player_stats)
      {:ok, %PlayerStats{}}

      iex> delete_player_stats(player_stats)
      {:error, %Ecto.Changeset{}}

  """
  def delete_player_stats(%PlayerStats{} = player_stats) do
    Repo.delete(player_stats)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking player_stats changes.

  ## Examples

      iex> change_player_stats(player_stats)
      %Ecto.Changeset{data: %PlayerStats{}}

  """
  def change_player_stats(%PlayerStats{} = player_stats, attrs \\ %{}) do
    PlayerStats.changeset(player_stats, attrs)
  end
end
