defmodule Nflrushing.Player.PlayerStats do
  use Ecto.Schema
  import Ecto.Changeset

  schema "player_stats" do
    field(:att, :integer)
    field(:att_g, :decimal)
    field(:avg, :decimal)
    field(:first, :decimal)
    field(:first_percentage, :decimal)
    field(:forty_plus, :integer)
    field(:fum, :integer)
    field(:lng, :string)
    field(:lng_num, :integer)
    field(:player, :string)
    field(:pos, :string)
    field(:td, :integer)
    field(:team, :string)
    field(:twenty_plus, :integer)
    field(:yds, :integer)
    field(:yds_g, :decimal)

    timestamps()
  end

  @doc false
  def changeset(player_stats, attrs) do
    player_stats
    |> cast(attrs, [
      :player,
      :team,
      :pos,
      :att,
      :att_g,
      :yds,
      :avg,
      :yds_g,
      :td,
      :lng,
      :first,
      :first_percentage,
      :twenty_plus,
      :forty_plus,
      :fum
    ])
    |> validate_required([
      :player,
      :team,
      :pos,
      :att,
      :att_g,
      :yds,
      :avg,
      :yds_g,
      :td,
      :lng,
      :first,
      :first_percentage,
      :twenty_plus,
      :forty_plus,
      :fum
    ])
    |> append_lng_num
  end

  defp append_lng_num(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{lng: lng}} ->
        put_change(
          changeset,
          :lng_num,
          lng |> String.replace("T", "") |> String.to_integer()
        )

      _ ->
        changeset
    end
  end
end
