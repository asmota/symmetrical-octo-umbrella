defmodule Nflrushing.Player.StatsSearch do
  @moduledoc """
  Implementation of the full-text player search with custom sorting.
  """

  alias Nflrushing.Repo
  alias Nflrushing.Player.PlayerStats

  import Ecto.Query

  def run(params, :all) do
    params |> generate_query() |> Repo.all()
  end

  def run_with_stream(params, callback) do
    Repo.transaction(fn ->
      stream = params |> generate_query() |> Repo.stream(max_rows: 500)
      callback.(stream)
    end)
  end

  def run(params) do
    params |> generate_query() |> Repo.paginate(params)
  end

  def generate_query(params) do
    from(
      s in PlayerStats,
      order_by: ^filter_order_by(params["order_by"])
    )
    |> (fn query ->
          case params["name"] do
            "" ->
              query

            [] ->
              query

            nil ->
              query

            _ ->
              where(
                query,
                fragment(
                  "to_tsvector('english', player) @@ to_tsquery(?)",
                  ^prefix_search(params["name"])
                )
              )
          end
        end).()
  end

  defp prefix_search(term), do: String.replace(term, ~r/\W/u, "") <> ":*"

  defp filter_order_by("yds_desc"), do: [desc: :yds]
  defp filter_order_by("yds_asc"), do: [asc: :yds]
  defp filter_order_by("lng_desc"), do: [desc: :lng_num]
  defp filter_order_by("lng_asc"), do: [asc: :lng_num]
  defp filter_order_by("td_desc"), do: [desc: :td]
  defp filter_order_by("td_asc"), do: [asc: :td]
  defp filter_order_by(_), do: []
end
